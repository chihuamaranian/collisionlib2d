﻿namespace LongHorse.CollisionLib2D
{
    public enum BoundingType
    {
        Circle,
        Rectangle
    }
}
